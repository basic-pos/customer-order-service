package com.pos.customerordermanagementservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class CustomerOrderManagementServiceApplication {

    /**
     * main method.
     *
     * @param args string array
     */
    public static void main(final String[] args) {
        SpringApplication
                .run(CustomerOrderManagementServiceApplication.class, args);
    }

}
