package com.pos.customerordermanagementservice.service;

import com.pos.customerordermanagementservice.model.Customer;

import java.util.List;

public interface CustomerService {

    /***
     * get all existing customers.
     * @return all customers.
     */
    List<Customer> getCustomers();

    /***
     * create new customer.
     * @param customerData  new customer data to be added to the database
     * @return created customer
     */
    Customer createCustomer(Customer customerData);

    /***
     * get particular customer.
     * @param id customer id for find particular customer
     * @return searched customer
     */
    Customer getCustomer(Long id);

    /***
     * update particular customer.
     * @param id customer id for existing particular customer to be updated
     * @param customerData customer data to be updated
     * @return updated customer
     */
    Customer updateCustomer(Long id, Customer customerData);
}
