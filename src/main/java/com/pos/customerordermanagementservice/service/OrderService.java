package com.pos.customerordermanagementservice.service;

import com.pos.customerordermanagementservice.model.Order;

import java.util.List;

public interface OrderService {

    /***
     * get all existing orders.
     * @return all orders placed
     */
    List<Order> getOrders();

    /***
     * create new order.
     * @param createOrderData order data to be added to the database.
     * @return order that added
     */
    Order createOrder(Order createOrderData);

    /***
     * get particular order by order id.
     * @param id order id for find particular order
     * @return searched order
     */
    Order getOrder(Long id);

    /***
     * update existing order.
     * @param id order id for find existing particular order to be updated
     * @param updateOrderData order data to be updated
     * @return updated order
     */
    Order updateOrder(Long id, Order updateOrderData);
}
