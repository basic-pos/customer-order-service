package com.pos.customerordermanagementservice.service.impl;

import com.pos.customerordermanagementservice.exception.CustomerNotFoundException;
import com.pos.customerordermanagementservice.model.Customer;
import com.pos.customerordermanagementservice.repository.CustomerRepo;
import com.pos.customerordermanagementservice.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    /***
     * dependency injection( CustomerServiceImpl depends on CustomerRepo).
     */
    @Autowired
    private CustomerRepo customerRepo;

    /***
     * get all existing customers.
     * @return all customers.
     */
    @Override
    public List<Customer> getCustomers() {
        return customerRepo.findAll();
    }

    /***
     * create new customer.
     * @param customerData  new customer data to be added to the database
     * @return created customer
     */
    public Customer createCustomer(final Customer customerData) {
        return customerRepo.save(customerData);
    }

    /***
     * get particular customer.
     * @param id customer id for find particular customer
     * @return searched customer
     */
    @Override
    public Customer getCustomer(final Long id) {
        //Optional returns an object(Entity object or unknown object or null.
        return customerRepo.findById(id)
                .orElseThrow(() -> new CustomerNotFoundException(
                        "can't find customer for this id", 450));
    }

    /***
     * update particular customer.
     * @param id customer id for existing particular customer to be updated
     * @param customerData customer data to be updated
     * @return updated customer
     */
    @Override
    public Customer updateCustomer(final Long id,
                                   final Customer customerData) {
        return customerRepo.findById(id)
                .map(customer -> customerRepo.save(customerData))
                .orElseThrow(() -> new CustomerNotFoundException(
                        "can't find customer for this id", 450));
    }
}
