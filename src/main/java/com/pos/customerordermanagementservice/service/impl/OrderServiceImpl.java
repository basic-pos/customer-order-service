package com.pos.customerordermanagementservice.service.impl;

import com.pos.customerordermanagementservice.exception.OrderNotFoundException;
import com.pos.customerordermanagementservice.model.Order;
import com.pos.customerordermanagementservice.repository.OrderRepo;
import com.pos.customerordermanagementservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    /***
     * dependency injection( OrderServiceImpl depends on OrderRepo).
     */
    @Autowired
    private OrderRepo orderRepo;

    /***
     * get all existing orders.
     * @return all orders placed
     */
    @Override
    public List<Order> getOrders() {
        return orderRepo.findAll();
    }

    /***
     * create new order.
     * @param createOrderData order data to be added to the database.
     * @return order that added
     */
    @Override
    public Order createOrder(final Order createOrderData) {
        return orderRepo.save(createOrderData);
    }

    /***
     * get particular order by order id.
     * @param id order id for find particular order
     * @return searched order
     */
    @Override
    public Order getOrder(final Long id) {
        return orderRepo.findById(id)
                .orElseThrow(() -> new OrderNotFoundException(
                        "can't find order for this id", 450));
    }

    /***
     * update existing order.
     * @param id order id for find existing particular order to be updated
     * @param updateOrderData order data to be updated
     * @return updated order
     */
    @Override
    public Order updateOrder(final Long id,
                             final Order updateOrderData) {
        return orderRepo.findById(id)
                .map(order -> orderRepo.save(updateOrderData))
                .orElseThrow(() -> new OrderNotFoundException(
                        "can't find Order for this id", 450));
    }
}
