package com.pos.customerordermanagementservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//@JsonInclude(JsonInclude.Include.NON_NULL) every field which has null value will be ignored/exclude any null field
//@JsonIgnoreProperties(value = {"firstName", "LastName"})
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@NoArgsConstructor
@Data
@Entity
@Table(name = "customer", schema = "public")
public class Customer implements Serializable {

    /**
     * customer id, primary key, auto generated.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false, unique = true)
    private Long id;

    /**
     * customer first name.
     */
    @Column(name = "first_name")
    private String firstName;

    /**
     * last name.
     */
    @Column(name = "last_name")
    private String lastName;

    /**
     * customer email.
     */
    @Column(name = "email")
    private String email;

    /**
     * customer contact.
     */
    @Column(name = "contact")
    private Integer contact;

    /**
     * customer postal code.
     */
    @Column(name = "postal_code")
    private Integer postalCode;

    /**
     * customer street.
     */
    @Column(name = "street")
    private String street;

    /**
     * customer city.
     */
    @Column(name = "city")
    private String city;

    /**
     * customer status.
     */
    @Column(name = "status")
    private Boolean status;

    /**
     * list of all orders that a customer placed.
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonManagedReference(value = "customerRef")
    @OneToMany(mappedBy = "customer",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Order> orders = new ArrayList<>();

}

// Jackson is a Java library to map Java objects to JSON
