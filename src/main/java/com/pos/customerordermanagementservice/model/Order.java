package com.pos.customerordermanagementservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.pos.customerordermanagementservice.audit.Auditable;
import lombok.NoArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.io.Serializable;

@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "orders", schema = "public")
public class Order extends Auditable<String> implements Serializable {

    /**
     * order id, primary key, auto generated.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Long id;

    /**
     * item id.
     */
    @Column(name = "item_id")
    private Long itemId;

    /**
     * item price.
     */
    @Column(name = "price")
    private Float price;

    /**
     * item quantity.
     */
    @Column(name = "qty")
    private Integer qty;

    /**
     * payment type.
     */
    @Column(name = "payment_type")
    private String paymentType;

    /**
     * customer id as a foreign key in order table.
     */
    @JoinColumn(name = "customer_id", updatable = false, nullable = false)
    @JsonBackReference(value = "customerRef")
    @ManyToOne
    private Customer customer;

}
