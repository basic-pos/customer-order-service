package com.pos.customerordermanagementservice.dto.rqst;

import com.pos.customerordermanagementservice.model.Order;
import lombok.Data;

import javax.validation.constraints.Size;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.AssertTrue;
import java.util.ArrayList;
import java.util.List;

@Data
public class CustomerRequestDto {

    public static final int maxFirstNameSize = 50;
    public static final int maxLastNameSize = 50;
    public static final int maxEmailSize = 50;
    public static final int minEmailSize = 5;
    public static final int maxContactSize = 10;
    public static final int minContactSize = 10;
    public static final int maxPostalCodeSize = 4;
    public static final int minPostalCodeSize = 4;
    public static final int maxStreetSize = 50;
    public static final int maxCitySize = 50;

    /**
     * customer first name.
     */
    @NotNull(message = "first name can't be null")
    @Max(value = maxFirstNameSize,
            message = "first name must be less than 50 characters")
    private String firstName;

    /**
     * last name.
     */
    @NotNull(message = "last name can't be null")
    @Max(value = maxLastNameSize,
            message = "last name must be less than 50 characters")
    private String lastName;

    /**
     * customer email.
     */
    @NotNull(message = "email can't be null")
    @Email(message = "email should be valid")
    @Size(min = minEmailSize, max = maxEmailSize,
            message = "email must be between 5 and 50 characters")
    private String email;

    /**
     * customer contact.
     */
    @Size(min = minContactSize, max = maxContactSize,
            message = "contact number must be 10 digits")
    @Pattern(regexp = "(^[0-9]{10})")
    private String contact;

    /**
     * customer postal code.
     */
    @NotNull(message = "postal card can't be null")
    @Size(min = minPostalCodeSize, max = maxPostalCodeSize,
            message = "postal code must be 4 digits")
    @Pattern(regexp = "(^[0-9]{4})")
    private String postalCode;

    /**
     * customer street.
     */
    @NotNull(message = "street can't be null")
    @Max(value = maxStreetSize,
            message = "street must be less than 50 characters")
    private String street;

    /**
     * customer city.
     */
    @NotNull(message = "city can't be null")
    @Max(value = maxCitySize,
            message = "city must be less than 50 characters")
    private String city;

    /**
     * customer status.
     */
    @NotNull(message = "status can't be null")
    @AssertTrue(message = "status must be true")
    private Boolean status;

    /**
     * list of all orders that a customer placed.
     */
    private List<Order> orders = new ArrayList<>();

}
