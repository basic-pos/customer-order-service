package com.pos.customerordermanagementservice.dto.rqst;

import com.pos.customerordermanagementservice.model.Customer;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class
OrderRequestDto {

    /**
     * item id.
     */
    @NotNull(message = "item id can't be null")
    @Pattern(regexp = "(^[0-9]{10})")
    private String itemId;

    /**
     * item price.
     */
    @NotNull(message = "price can't be null")
    @Pattern(regexp = "(^[0-9]{10})")
    private String price;

    /**
     * item quantity.
     */
    @NotNull(message = "qty can't be null")
    @Pattern(regexp = "(^[0-9]{10})")
    private String qty;

    /**
     * payment type.
     */
    @NotNull(message = "payment type can't be null")
    private String paymentType;

    /**
     * customer id as a foreign key in order table.
     */
    @NotNull(message = "customer can't be null")
    private Customer customer;

}
