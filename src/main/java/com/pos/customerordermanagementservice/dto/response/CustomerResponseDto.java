package com.pos.customerordermanagementservice.dto.response;

import lombok.Data;

@Data
public class CustomerResponseDto {

    /**
     * customer first name.
     */
    private String firstName;

    /**
     * last name.
     */
    private String lastName;

    /**
     * customer email.
     */
    private String email;

    /**
     * customer contact.
     */
    private Integer contact;

    /**
     * customer postal code.
     */
    private Integer postalCode;

    /**
     * customer street.
     */
    private String street;

    /**
     * customer city.
     */
    private String city;

    /**
     * customer status.
     */
    private Boolean status;
}
