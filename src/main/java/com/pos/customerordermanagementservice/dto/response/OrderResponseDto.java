package com.pos.customerordermanagementservice.dto.response;

import lombok.Data;

@Data
public class OrderResponseDto {

    /**
     * item id.
     */
    private Long itemId;

    /**
     * item price.
     */
    private Float price;

    /**
     * item quantity.
     */
    private Integer qty;

    /**
     * payment type.
     */
    private String paymentType;
}
