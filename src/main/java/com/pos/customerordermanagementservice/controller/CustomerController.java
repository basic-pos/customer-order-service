package com.pos.customerordermanagementservice.controller;

import com.pos.customerordermanagementservice.dto.rqst.CustomerRequestDto;
import com.pos.customerordermanagementservice.model.Customer;
import com.pos.customerordermanagementservice.service.CustomerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@Validated
@RequestMapping("/customers")
public class CustomerController {

    /***
     * dependency injection( CustomerController depends on CustomerService).
     */
    @Autowired
    private CustomerService customerService;

    /***
     * get all existing customers.
     * @return all customers
     */
    @GetMapping
    public List<Customer> getCustomers() {
        return customerService.getCustomers();
    }

    /***
     * create new customer.
     * @param customerData  new customer data to be added to the database
     * @return created customer
     */
    @PostMapping
    public Customer createCustomer(@Valid @RequestBody final CustomerRequestDto customerData) {
        ModelMapper modelMapper = new ModelMapper();
        Customer persistenceCustomerData = modelMapper.map(customerData, Customer.class);
        return customerService.createCustomer(persistenceCustomerData);
    }

    /***
     * get particular customer.
     * @param id customer id for find particular customer
     * @return searched customer
     */
    @GetMapping(value = "/{id}")
    public Customer getCustomer(@PathVariable(value = "id") @Min(1) final Long id) {
        return customerService.getCustomer(id);
    }

    /***
     * update particular customer.
     * @param id customer id for existing particular customer to be updated
     * @param updateCustomerData customer data to be updated
     * @return updated customer
     */
    @PutMapping(value = "/{id}")
    public Customer updateCustomer(@PathVariable(value = "id") @Min(1) final Long id,
                                   @Valid @RequestBody final CustomerRequestDto updateCustomerData) {
        ModelMapper modelMapper = new ModelMapper();
        Customer persistenceCustomerData = modelMapper.map(updateCustomerData, Customer.class);
        return customerService.updateCustomer(id, persistenceCustomerData);
    }

}
