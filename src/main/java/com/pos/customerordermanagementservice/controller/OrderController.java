package com.pos.customerordermanagementservice.controller;

import com.pos.customerordermanagementservice.dto.rqst.OrderRequestDto;
import com.pos.customerordermanagementservice.model.Order;
import com.pos.customerordermanagementservice.service.OrderService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {

    /***
     * dependency injection( OrderController depends on OrderService).
     */
    @Autowired
    private OrderService orderService;

    /***
     * get all existing orders.
     * @return all orders placed
     */
    @GetMapping
    public List<Order> getOrders() {
        return orderService.getOrders();
    }

    /***
     * create new order.
     * @param createOrderData order data to be added to the database.
     * @return order that added
     */
    @PostMapping
    public Order createOrder(@RequestBody final OrderRequestDto createOrderData) {
        ModelMapper modelMapper = new ModelMapper();
        Order persistenceOrderData = modelMapper.map(createOrderData, Order.class);
        return orderService.createOrder(persistenceOrderData);
    }

    /***
     * get particular order by order id.
     * @param id order id for find particular order
     * @return searched order
     */
    @GetMapping("/{id}")
    public Order getOrder(@PathVariable(value = "id") @Min(1) final Long id) {
        return orderService.getOrder(id);
    }

    /***
     * update existing order.
     * @param id order id for find existing particular order to be updated
     * @param updateOrderData order data to be updated
     * @return updated order
     */
    @PutMapping(value = "/{id}")
    public Order updateOrder(@PathVariable(value = "id") @Min(1) final Long id,
                             @RequestBody final OrderRequestDto updateOrderData) {
        ModelMapper modelMapper = new ModelMapper();
        Order persistenceOrderData = modelMapper.map(updateOrderData, Order.class);
        return orderService.updateOrder(id, persistenceOrderData);
    }

}
