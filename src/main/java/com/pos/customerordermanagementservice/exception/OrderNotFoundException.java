package com.pos.customerordermanagementservice.exception;

import lombok.Data;

@Data
public class OrderNotFoundException extends RuntimeException {

    /**
     * exception message
     */
    private final String message;

    /**
     * response status code
     */
    private final int status;

    /***
     * all args constructor
     * @param message exception msg
     * @param status response status code
     */
    public OrderNotFoundException(String message, int status) {
        super(message);
        this.message = message;
        this.status = status;
    }
}
