package com.pos.customerordermanagementservice.exception.handler;

import com.pos.customerordermanagementservice.exception.CustomerNotFoundException;
import com.pos.customerordermanagementservice.exception.OrderNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/***
 * handle global exceptions
 * @author kalana sandakelum
 */
@Log4j2
@ControllerAdvice
public class GlobalExceptionHandler {

    static final String MSG = "exception found : {}";

    /***
     * handle default exception.
     * @param exception exception
     * @return response entity with exception msg and http status
     */
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Object> handleException(Exception exception) {
        log.error(MSG, exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(),
                HttpStatus.NOT_FOUND);
    }

    /***
     * handle customer not found exceptions.
     * @param exception passed exception
     * @return response entity with exception msg and http status
     */
    @ExceptionHandler(value = {CustomerNotFoundException.class})
    public ResponseEntity<Object> handleCustomerNotFoundException(
            CustomerNotFoundException exception) {
        log.error(MSG, exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    /***
     * handle order not found exceptions.
     * @param exception passed exception
     * @return response entity with exception msg and http status
     */
    @ExceptionHandler(value = {OrderNotFoundException.class})
    public ResponseEntity<Object> handleOrderNotFoundException(
            OrderNotFoundException exception) {
        log.error(MSG, exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(),
                HttpStatus.NOT_FOUND);
    }

}
