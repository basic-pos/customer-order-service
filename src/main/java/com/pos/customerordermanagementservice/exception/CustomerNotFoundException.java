package com.pos.customerordermanagementservice.exception;

import lombok.Data;

@Data
public class CustomerNotFoundException extends RuntimeException {

    /**
     * exception message
     */
    private final String message;

    /**
     * response status code
     */
    private final int status;

    /***
     * all args constructor
     * @param message exception msg
     * @param status response status code
     */
    public CustomerNotFoundException(String message, int status) {
        super(message);
        this.message = message;
        this.status = status;
    }
}
