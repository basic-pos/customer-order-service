package com.pos.customerordermanagementservice.repository;

import com.pos.customerordermanagementservice.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepo extends JpaRepository<Customer, Long> {
}
