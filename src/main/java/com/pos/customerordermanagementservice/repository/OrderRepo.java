package com.pos.customerordermanagementservice.repository;

import com.pos.customerordermanagementservice.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepo extends JpaRepository<Order, Long> {
}
